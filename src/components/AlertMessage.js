import React, {Component} from 'react';
import {Alert} from 'react-bootstrap';

export class AlertMessage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            alertVisible: true
        }
    }

    componentDidMount(){
        var removeAlert = this.props.removeAlert;
        setTimeout(function() {
            removeAlert();
        }, 5000)
    }

    render() {
        const { message, alertStyle } = this.props;
        var highlight;
        if(alertStyle == "success") highlight="Success!";
        else highlight="Error!";
        if (this.state.alertVisible) {
            return (
                <Alert bsStyle={ alertStyle }>
                    <p><b> { highlight } </b> { message } </p>
                </Alert>
            );
        }
        else{
            return(
                <div></div>
            );
        }
    }
}

export default AlertMessage;