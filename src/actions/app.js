import { BOOKSTORE_API } from '../constants/action-types'
import { GET_BOOKSTORES } from '../constants/action-types'
import { GET_INVENTORIES } from '../constants/action-types'
import { GET_INVENTORIES_API } from '../constants/action-types'
import { GET_BOOKS } from '../constants/action-types'
import { GET_BOOKS_API, DELETE_ITEM } from '../constants/action-types'
import { UPDATE_CART, PLACE_ORDER } from '../constants/action-types';
import fetch from 'isomorphic-fetch';

function SelectBookStore(data) {
    return {
        type: GET_BOOKSTORES,
        data: data
    }
}

function SelectBooks(data){
    return {
        type: GET_BOOKS,
        data: data
    }
}

function SelectInventories(data){
    return {
        type: GET_INVENTORIES,
        data: data
    }
}

export function GetBookstores (){
    return function(dispatch){
        return fetch(BOOKSTORE_API)
            .then((response) => {  return response.json(); })
            .then((json) => { return dispatch(SelectBookStore(json))})
    }
}

export function GetBooks(storeID) {
    return function(dispatch) {
        return fetch(GET_BOOKS_API)
            .then((response) => { return response.json(); })
            .then((json) => { return dispatch(SelectBooks(json))})
    }
}

export function GetInventories(){
    return function(dispatch) {
        return fetch(GET_INVENTORIES_API)
            .then((response) => { return response.json(); })
            .then((json) => { return dispatch(SelectInventories(json))})
    }
}

export function UpdateCart(id, quantity) {
        return {
            type: UPDATE_CART,
            id: id,
            quantity: quantity
        }
}

export function DeleteItem(id){
    return {
        type: DELETE_ITEM,
        id: id
    }
}

export function PlaceOrder(){
    return {
        type: PLACE_ORDER
    }
}