import React from 'react';
import { Router, Route, hashHistory } from 'react-router'
import App from 'containers/app';
import Cart from 'containers/Cart'
import Bookstore from './containers/Bookstore'

export const routes = (
    <Router history={hashHistory}>
        <Route path="/" component={App} >
            <Route path="/bookstore/:id" component={Bookstore} />
        </Route>
    </Router>
);

export default routes;
