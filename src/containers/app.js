import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import styles from './app.css';
import { GetBookstores, GetInventories, InitCart } from '../actions/app';
import { Link } from 'react-router';

export class App extends Component {

  static propTypes = {
    stores: PropTypes.object
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {stores} = this.props;
    var render = [];
    for (var store in stores) {
      render.push(
          <li key={stores[store].id}>
            <Link activeStyle={{ color: 'red', fontWeight: 'bold'}} to={`/bookstore/${stores[store].id}`}> {stores[store].name} </Link>
          </li>
      );
    }
    
    return (
        <div className={styles.container}>
          <h2> Bookstores </h2>
            <h4> Select a bookstore</h4>
            { render }
          <hr />
          { this.props.children }
        </div>
    );
  }

  componentDidMount() {
    const {dispatch} = this.props;
    dispatch(GetBookstores());
      dispatch(GetInventories());
  }
}

function mapStateToProperties(state) {
  const data = state.app.bookstore;
  return {
    stores: data.stores,
    inventories: data.inventories,
    cart: data.cart
  };
}

export default connect(mapStateToProperties)(App);


