import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Button, Alert } from 'react-bootstrap';
import { DeleteItem, PlaceOrder } from '../actions/app'


export class Cart extends Component {

    constructor(props) {
        super(props);

        this.state = {
            alertVisible: false
        }
    }

    render() {
        const inventory = this.props.inventories;
        const cart = this.props.cart;
        var items = [];

        if (Object.keys(cart).length != 0) {

            {
                cart.map((obj) => {
                    items.push(
                        <tr key={ parseInt(Object.keys(obj)) }>
                            <td> { inventory[parseInt(Object.keys(obj)[0]) - 1].book.title } </td>
                            <td> { inventory[parseInt(Object.keys(obj)[0]) - 1].book.author } </td>
                            <td> { obj[Object.keys(obj)] }</td>
                            <td> { inventory[parseInt(Object.keys(obj)[0]) - 1].location.name }</td>
                            <td> <img src ={"https://cdn2.iconfinder.com/data/icons/diagona/icon/16/101.png"} onClick={ this.removeItem.bind(this, Object.keys(obj)) }/></td>
                        </tr>
                    )
                })
            }
            return (
                <div>
                    <h2> My Cart </h2>
                    <Table striped bordered condensed hover>
                        <thead>
                        <tr>
                            <td>
                                <th>Title</th>
                            </td>
                            <td>
                                <th>Author</th>
                            </td>
                            <td>
                                <th>Quantity</th>
                            </td>
                            <td>
                                <th>Pick Up location</th>
                            </td>
                            <td> Remove Item</td>
                        </tr>
                        </thead>
                        <tbody>
                        { items }
                        </tbody>
                    </Table>
                    <Button bsStyle="primary" onClick = { this.PlaceOrder.bind(this)}> Place pickup order!</Button>
                </div>
            )
        }
        else{
            return(
                <div>
                    { this.state.alertVisible ? <Alert bsStyle="info"> Order placed successfully! Resetting Shopping cart...</Alert> : null }
                    <h2> My Cart </h2>
                    <div> Nothing in the cart yet!</div>
                </div>
            )
        }
    }

    removeItem (id){
        const { dispatch } = this.props;
        dispatch(DeleteItem(id));
    }

    PlaceOrder(){

        this.setState({
           alertVisible: true
        });

        const { dispatch } = this.props;
        dispatch(PlaceOrder());
        var that = this;
        setTimeout(function() {
            that.setState({alertVisible: false});
        }, 3000)
    }
}

function mapStateToProperties(state) {
    const data = state.app.bookstore;
    return {
        inventories: data.inventories,
        cart: data.cart
    };
}

export default connect(mapStateToProperties)(Cart);