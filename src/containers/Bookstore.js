import React, { Component } from 'react';
import { GetBooks, InitCart, UpdateCart, DeleteItem } from '../actions/app'
import { connect } from 'react-redux'
import { Table, Button} from 'react-bootstrap';
import { update } from 'react-addons-update';
import { AlertMessage } from '../components/AlertMessage';
import Cart from '../containers/Cart';

export class Bookstore extends Component {

    constructor(props) {
        super(props);
        this.state = {
            books: [],
            error: false,
            errorMessage: "",
            alertStyle: "danger"
        }
    }
    
    render() {

        const storeID = this.props.params.id;
        var inventory = this.props.inventories;

        inventory = inventory
            .filter((obj) => {
                if(obj.location.id == storeID){
                    return true;
                }
            })
            .map((obj) => {
                return (
                    <tr key={obj.id}>
                        <td>{ obj.book.title }</td>
                        <td>{ obj.book.author }</td>
                        <td><input type="text" value={ this.state.books[obj.id] } onChange={ this.onChangeHandler.bind(this, obj.id) }/></td>
                        <td> { obj.quantity }</td>
                        <td><Button bsStyle = "primary" onClick={ this.onClickHandler.bind(this, obj.id)}> Add to Cart</Button></td>
                    </tr>
                )
            });

        return (
            <div>
                { this.state.error ? <AlertMessage message={ this.state.errorMessage } alertStyle= { this.state.alertStyle } removeAlert = { this.removeAlert.bind(this)}/> : null }
                <Table striped bordered condensed hover>
                    <thead>
                        <tr>
                            <td><th>Title</th></td>
                            <td><th>Author</th></td>
                            <td><th>Quantity</th></td>
                            <td><th>Available</th></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            inventory
                        }
                    </tbody>
                </Table>


                <Cart inventories={ this.props.inventories } cart={ this.props.cart } />

            </div>
        )
    }

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(GetBooks(this.props.params.id));

    }

    removeAlert(){
        this.setState ({
          error:false
        })

    }

    onClickHandler(id, event){
        const { dispatch, inventories } = this.props;

        if(parseInt(this.state.books[id]) > inventories[id-1].quantity){
            this.setState ({
                error: true,
                errorMessage: this.state.books[id]+" copies of this book are not available at the store!",
                alertStyle: "danger",
                books: {
                    [id]: ""
                }
            });
        }
        else {

            if (this.state.books[id] != '' && this.state.books[id] != undefined) {
                this.setState({
                    error: true,
                    errorMessage: "Item added to cart!",
                    alertStyle: "success",
                    books: {
                        [id]: ""
                    }
                });
                dispatch(UpdateCart(id, this.state.books[id]));
            }
            else {
                this.setState({
                    error: true,
                    errorMessage: "Quantity is empty, cannot add to cart!",
                    alertStyle: "danger",
                        books: {
                            [id]: ""
                        }
                    }
                );
            }
        }
    }

    onChangeHandler(bookID, event){
        if(/^[0-9]*$/.test(event.target.value)) {
            debugger;
            this.setState(
                {
                    books:{
                        [bookID]: event.target.value
                    }
                }
            );
        }
        else{
            this.setState(
                {
                    error: true,
                    errorMessage: "Quantity must be a number, cannot add to cart!",
                    alertStyle: "danger",
                    books: {
                        [bookID]:''
                    }
                }
            )
        }
    }
}

function mapStateToProperties(state) {
    const data = state.app.bookstore;
    return {
        inventories: data.inventories,
        cart: data.cart
    };
}

export default connect(mapStateToProperties)(Bookstore);