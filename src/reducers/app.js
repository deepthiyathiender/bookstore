import {GET_BOOKSTORES} from '../constants/action-types'
import {GET_BOOKS} from '../constants/action-types'
import {GET_INVENTORIES, UPDATE_CART, DELETE_ITEM, PLACE_ORDER} from '../constants/action-types'

const initialState = {
    bookstore: {
        stores: {},
        books: {},
        inventories: {},
        cart: {}
    }
};


const app = (state = initialState, action) => {
    switch (action.type) {
        case GET_BOOKSTORES:
            return {
                ...state,
                bookstore: {
                    stores: action.data,
                    books: [...state.bookstore.books],
                    inventories: [...state.bookstore.inventories],
                    cart: [...state.bookstore.cart]
                }
            };

        case GET_BOOKS:
            return {
                ...state,
                bookstore: {
                    stores: [...state.bookstore.stores],
                    books: action.data,
                    inventories: [...state.bookstore.inventories],
                    cart: [...state.bookstore.cart]
                }
            };
        case GET_INVENTORIES:
            return {
                ...state,
                bookstore: {
                    stores: [...state.bookstore.stores],
                    books: [...state.bookstore.books],
                    inventories: action.data,
                    cart: [...state.bookstore.cart]
                }
            };

        case UPDATE_CART:
            var book = {[action.id]: action.quantity};
            return {
                ...state,
                bookstore: {
                    stores: [...state.bookstore.stores],
                    books: [...state.bookstore.books],
                    inventories: [...state.bookstore.inventories],
                    cart: [...state.bookstore.cart, book]
                }
            };


        case DELETE_ITEM:
            var cart = state.bookstore.cart;
            var newCart = cart.filter((item) => {
                if(Object.keys(item)[0] != action.id[0]) {
                    return true;
                }
            });

            return {
                ...state,
                bookstore: {
                    stores: [...state.bookstore.stores],
                    books: [...state.bookstore.books],
                    inventories: [...state.bookstore.inventories],
                    cart: newCart
                }
            };

        case PLACE_ORDER:
            var inventories = state.bookstore.inventories;
            var cart = state.bookstore.cart;
            var Cartmap = {};
            cart.map((item) => {
                Cartmap[Object.keys(item)] = item[Object.keys(item)];
            });

            var newInventory = inventories.map((item) => {
                debugger;
                if(Cartmap.hasOwnProperty(item.id)){
                    var noOfBooks = parseInt(Cartmap[item.id]);
                    return {
                        ...item,
                        quantity: item.quantity - noOfBooks
                    }
                }
                else{
                    return {
                        ...item
                    }
                }
            });
        debugger;
        return {
            ...state,
            bookstore: {
                stores: [...state.bookstore.stores],
                books: [...state.bookstore.books],
                inventories: newInventory,
                cart: {}
            }
        };

        default: return state;
    }
};


export default app;